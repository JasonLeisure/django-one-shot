from django.contrib import admin
from .models import TodoList, TodoItem


class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date")


class TodoListAdmin(admin.ModelAdmin):
    list_display = ("name", "created_on")


admin.site.register(TodoList, TodoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
